- Robot: [Trident](https://www.openrov.com/products/trident/) (ROV), by OpenROV.
- New functionalities not provided by OpenROV (under development):
    - Alternative to the OpenROV Cockpit App to communicate with the ROV (DDS communication).

- Future:
    - ROS integration.
    - Integration of a 360 camera and VR headset.