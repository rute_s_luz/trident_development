This data was obtained with the "Admin Console" (Tools tab) of "RTI Connext" (good visualization and debug tool for dds messages).
____
# TRIDENT COMMUNICATION:

### Domain id 
    0

Publishers and Subscribers can only talk to each other if their Participants belong to the same
DomainId

### Host (Trident):
    220959a 

### Process (ID):
    
##### trident-control (163)
###### Publisher (Type name)
    - pid_setpoint_current          (orov::control::pid::Setpoint)
    - pid_state                     (orov::control::pid::State)
    - rov_controller_state_current  (orov::control::ControllerStatus)
    - rov_logging                   (orov::logging::LogMessage)
    - rov_safety                    (orov::control::safety::Safety)
    - trident_command_target        (orov::control::TridentMotorCommand)

###### Subscriber
    - pid_parameters_requested       (orov::control::pid::Parameters)
    - pid_setpoint_requested         (orov::control::pid::Setpoint)
    - rov_attitude                   (orov::Attitude)
    - rov_control_target             (orov::TridentControl)
    - rov_controller_state_requested (orov::control::ControllerStatus)
    - rov_depth                      (orov::Depth)
    - rov_motor_command_debug        (orov::control::TridentMotorCommand)

#####  trident-record (168)
###### Publisher
    - rov_recording_stats           (orov::recording::RecordingStats)
    - rov_vid_session_current       (orov::recording::VideoSession)
    - rov_vid_session_rep           (orov::recording::VideoSessionCommand)

###### Subscriber
    - rov_cam_fwd_H264_1_video      (orov::image::VideoData)
    - rov_vid_session_req           (orov::recording::VideoSessionCommand)

#####  trident-update (169)
###### Publisher
    - rov_firmware_command_rep      (orov::system::FirmwareCommand)
    - rov_firmware_service_status   (orov::system::FirmwareServiceStatus)
    - rov_firmware_status           (orov::system::FirmwareStatus)

###### Subscriber
    - rov_firmware_command_req      (orov::system::FirmwareCommand)
    - rov_mcu_status                (orov::system::MCUStatus)
    
##### trident-core (170)
###### Publisher
    - rov_attitude                  (orov::Attitude)
    - rov_beacon                    (orov::ROVBeacon)
    - rov_depth                     (orov::Depth)
    - rov_esc_fault_alert           (orov::control::ESCFaultAlert)
    - rov_esc_fault_warning_info    (orov::control::ESCFaultWarningInfo)
    - rov_esc_feedback              (orov::control::ESCFeedback)
    - rov_fuelgauge_health          (orov::FuelgaugeHealth)
    - rov_fuelgauge_status          (orov::FuelgaugeStatus)
    - rov_imu_calibration           (orov::IMUCalibration)
    - rov_light_power_current       (orov::LightPower)
    - rov_mcu_comm_stats            (orov::CommStats)
    - rov_mcu_i2c_stats             (orov::I2CStats)
    - rov_mcu_status                (orov::system::MCUStatus)
    - rov_mcu_watchdog_status       (orov::system::MCUWatchdogStatus)
    - rov_ping_reply                (DDS::String)
    - rov_pressure_internal         (orov::Barometer)
    - rov_subsystem_status          (orov::system::SubsystemStatus_v2_0)
    - rov_temp_internal             (orov::Temperature)
    - rov_temp_water                (orov::Temperature)
    - rov_vactest_state_current     (orov::VacuumTest)
    - rov_vactest_state_data        (orov::VacuumTest)
    
###### Subscriber
    - rov_light_power_requested     (orov::LightPower)
    - rov_ping_request              (DDS::String)
    - rov_vactest_blink_command     (orov::Command)
    - rov_vactest_state_requested   (orov::VacuumTest)
    - rov_command_target            (orov::control::TridentMotorCommand)
##### geoserve (171)
###### Publisher
    - rov_cam_fwd                       (orov::image::Channel)
    - rov_cam_fwd_H264_0_ctrl_current   (orov::image::ControlValue)
    - rov_cam_fwd_H264_0_ctrl_desc      (orov::image::ControlDescriptor)
    - rov_cam_fwd_H264_0_video          (orov::image::VideoData)
    - rov_cam_fwd_H264_1_ctrl_current   (orov::image::ControlValue)
    - rov_cam_fwd_H264_1_ctrl_desc      (orov::image::ControlDescriptor)
    - rov_cam_fwd_H264_1_video          (orov::image::VideoData)
    - rov_cams                          (orov::image::Camera)

###### Subscriber
    - rov_cam_fwd_H264_0_ctrl_requested     (orov::image::ControlValue)
    - rov_cam_fwd_H264_1_ctrl_requested     (orov::image::ControlValue)









