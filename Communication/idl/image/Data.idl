#ifndef __orov__msg__image__Data__idl__
#define __orov__msg__image__Data__idl__

module orov {
module image {

    const unsigned long VIDEO_DATA_MAX_SIZE = 1000 * 1024;           // 1MB
    const unsigned long IMAGE_DATA_MAX_SIZE = 12 * 1024 * 1024;     // 12MB

    struct VideoData
    {
        unsigned long long timestamp;
        unsigned long long frame_id;
        sequence<octet, VIDEO_DATA_MAX_SIZE> data;
    };

    // Topic format: "<topicPrefix_rov_camChannels><channel_id>_ctrl_video"
    // Ex: rov_cam_forward_H2640_video
    struct ImageData
    {
        unsigned long long timestamp;
        unsigned long long frame_id;
        sequence<octet, IMAGE_DATA_MAX_SIZE> data;
    };

    struct VideoStats
    {
        unsigned long   average_bitrate;        // Average calculated bitrate
        unsigned long   min_frame_size;         // Smallest frame seen
        unsigned long   max_frame_size;         // Largest frame seen
        unsigned long   dropped_frames;         // Number of frames not even sent to readers, usually due to slow servicing of the camera or frame queues
        unsigned long   lost_frames;            // Number of frames lost between the reader and writer. Usually due to poor connection
        unsigned long   est_camera_latency;     // Estimated latency component due to the camera's processing pipeline
        unsigned long   est_processing_latency; // Estimated latency component due to server software that moves data from camera to reader
        unsigned long   est_network_latency;    // Estimated latency component due to transmission over the network, which includes reliability protocol and things like wifi retry
        unsigned long   est_total_latency;      // Estimated total latency, sum of above
        float           fps;                    // Calculated FPS, based on timestamps received directly from camera
    };
    
};
};

#endif