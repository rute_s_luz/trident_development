#ifndef __orov__msg__recording__Recording__idl__
#define __orov__msg__recording__Recording__idl__

module orov {
module msg {
module recording {

    module trident {
        const string RECORDING_SUBSYSTEM_VIDEO      = "video";
        const string RECORDING_SUBSYSTEM_TELEMETRY  = "telemetry";
        const string RECORDING_SUBSYSTEM_LOGS       = "logs";
    };

    enum EVideoSessionCommandResponse
    {
        UNKNOWN                         = 0,
        ACCEPTED                        = 1,    // Session request was accepted
        REJECTED_GENERIC                = 2,    // Placeholder for any rejection reason not listed below
        REJECTED_SESSION_IN_PROGRESS    = 3,    // There is already a recording in progress. Current session needs to be stopped.
        REJECTED_INVALID_SESSION        = 4,    // There is no active session to be stopped with this ID
        REJECTED_NO_SPACE               = 5     // There is not enough space to begin a new recording
    };

    enum EVideoSessionState
    {
        UNKNOWN     = 0,
        RECORDING   = 1,    // Actively recording video
        STOPPED     = 2     // Recording stopped, either intentionally or due to an error while recording
    };

    enum EVideoSessionSubstate
    {
        UNKNOWN             = 0, // Initial state
        STOPPED_PROBING     = 1, // Recorder is waiting for an IDR frame to parse stream information
        STOPPED_ALIGNING    = 2, // Recorder is waiting for the next key frame
        STOPPED_READY       = 3, // Recorder is ready to begin a recording session and is circulating one or more GOPs
        RECORDING           = 4, // Recorder is actively recording frames to disk
        STOPPED_ERROR       = 5, // Error has occurred and must be dealt with (either recoverable or fatal)
        STOPPED_TERMINATED  = 6  // Recording thread has terminated due to explicit termination signal or unexpected fatal error
    };

    enum EVideoSessionStopReason
    {
        UNKNOWN                     = 0,
        CLIENT_REQUEST              = 1, // User explicitly requested recording stop
        CLIENT_NOT_ALIVE            = 2, // There are no clients publishing to the session request topic (explicitly disconnected, crashed, extended network interruption)
        VIDEO_SOURCE_NOT_ALIVE      = 3, // There is no source of video data publishing on the topic being recorded (crashed, timed out, etc.)
        FILESYSTEM_NOSPACE          = 4, // Ran out of space for video.
        MAX_SESSION_SIZE_REACHED    = 5, // Reached the max configured byte size for a session and automatically stopped the recording.
        RECORDING_ERROR             = 6  // An error occurred in the recording process (muxing, creating a file, etc)
    };

    enum EFilesystemProvisioningState {
        UNKNOWN         = 0,
        UNPROVISIONED   = 1, // Filesystem has not been provisioned yet.
        PROVISIONED     = 2, // Filesystem for the recording subsystem has been successfully provisioned. See disk_space_total_bytes for size.
        ERROR           = 3  // Filesystem can not be provisioned. See error_reason for more info
    };

    struct VideoSessionCommand
    {
        // Requester info
        string<128> session_id;                 // If trying to stop a session, the ID should be provided here. If starting a session, leave blank. The vehicle will create the ID.
        string<512> metadata;                   // Any additional information that should be associated with the recording if accepted (date/time, etc)
        EVideoSessionState request;             // Requested target state (recording or stopped)

        // Replier info
        EVideoSessionCommandResponse response;  // Contains the ACK/NACK response to the request
        string reason;                          // Additional NACK information
    };

    struct VideoSession
    {
        string<128> session_id;                 // If trying to stop a session, the ID should be provided here. If starting a session, leave blank
        string<512> metadata;                   // Any additional information that should be associated with the recording if accepted (date/time, etc)

        // Session state
        EVideoSessionState state;               // Current state of the session
        EVideoSessionStopReason stop_reason;    // If in STOPPED state, the last reason for stopping

        unsigned long segment_count;            // Current number of segments in this session
        unsigned long total_duration_s;         // Current total length of recording, rounded to seconds
        unsigned long long total_size_bytes;    // Total disk space used by this session

        EVideoSessionSubstate substate;         // Current substate of the session
    };
    
    struct RecordingStats
    {
        // See: trident::RECORDING_SUBSYSTEM_X constants
        string<128> rec_subsys_id; //@key

        unsigned long long disk_space_total_bytes;  // Total space allocated for this subsystem
        unsigned long long disk_space_used_bytes;   // Space currently used up by subsystem
        unsigned long est_remaining_rec_time_s;     // Estimate of remaining recording time available at current bandwidths

        EFilesystemProvisioningState provisioning_state;
        string<128> error_reason;
    };

};
};
};

#endif