// Copyright 2016 Proyectos y Sistemas de Mantenimiento SL (eProsima).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*! 
 * @file Depth.cpp
 * This source file contains the definition of the described types in the IDL file.
 *
 * This file was generated by the tool gen.
 */

#ifdef _WIN32
// Remove linker warning LNK4221 on Visual Studio
namespace { char dummy; }
#endif

#include "Depth.h"
#include <fastcdr/Cdr.h>

#include <fastcdr/exceptions/BadParamException.h>
using namespace eprosima::fastcdr::exception;

#include <utility>





orov::Depth::Depth()
{


    m_depth = 0.0;


}

orov::Depth::~Depth()
{
}

orov::Depth::Depth(const Depth &x)
{
    m_pressure = x.m_pressure;
    m_id = x.m_id;
    m_depth = x.m_depth;
}

orov::Depth::Depth(Depth &&x)
{
    m_pressure = std::move(x.m_pressure);
    m_id = std::move(x.m_id);
    m_depth = x.m_depth;
}

orov::Depth& orov::Depth::operator=(const Depth &x)
{
    m_pressure = x.m_pressure;
    m_id = x.m_id;
    m_depth = x.m_depth;

    return *this;
}

orov::Depth& orov::Depth::operator=(Depth &&x)
{
    m_pressure = std::move(x.m_pressure);
    m_id = std::move(x.m_id);
    m_depth = x.m_depth;

    return *this;
}

size_t orov::Depth::getMaxCdrSerializedSize(size_t current_alignment)
{
    size_t initial_alignment = current_alignment;

    current_alignment += orov::msg::sensor::FluidPressure::getMaxCdrSerializedSize(current_alignment);
    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4) + 255 + 1;

    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4);



    return current_alignment - initial_alignment;
}

size_t orov::Depth::getCdrSerializedSize(const orov::Depth& data, size_t current_alignment)
{
    (void)data;
    size_t initial_alignment = current_alignment;

    current_alignment += orov::msg::sensor::FluidPressure::getCdrSerializedSize(data.pressure(), current_alignment);
    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4) + data.id().size() + 1;

    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4);



    return current_alignment - initial_alignment;
}

void orov::Depth::serialize(eprosima::fastcdr::Cdr &scdr) const
{
    scdr << m_pressure;
    scdr << m_id;
    scdr << m_depth;
}

void orov::Depth::deserialize(eprosima::fastcdr::Cdr &dcdr)
{
    dcdr >> m_pressure;
    dcdr >> m_id;
    dcdr >> m_depth;
}

size_t orov::Depth::getKeyMaxCdrSerializedSize(size_t current_alignment)
{
	size_t current_align = current_alignment;
            

     current_align += 4 + eprosima::fastcdr::Cdr::alignment(current_align, 4) + 255 + 1;
     


    return current_align;
}

bool orov::Depth::isKeyDefined()
{
    return true;
}

void orov::Depth::serializeKey(eprosima::fastcdr::Cdr &scdr) const
{
	(void) scdr;
	 
	 scdr << m_id;  
	 
}
orov::DepthConfig::DepthConfig()
{

    m_water_type = 0;

    m_user_offset_enabled = false;

    m_zero_offset = 0.0;

    m_zero_offset_user = 0.0;


}

orov::DepthConfig::~DepthConfig()
{
}

orov::DepthConfig::DepthConfig(const DepthConfig &x)
{
    m_id = x.m_id;
    m_water_type = x.m_water_type;
    m_user_offset_enabled = x.m_user_offset_enabled;
    m_zero_offset = x.m_zero_offset;
    m_zero_offset_user = x.m_zero_offset_user;
}

orov::DepthConfig::DepthConfig(DepthConfig &&x)
{
    m_id = std::move(x.m_id);
    m_water_type = x.m_water_type;
    m_user_offset_enabled = x.m_user_offset_enabled;
    m_zero_offset = x.m_zero_offset;
    m_zero_offset_user = x.m_zero_offset_user;
}

orov::DepthConfig& orov::DepthConfig::operator=(const DepthConfig &x)
{
    m_id = x.m_id;
    m_water_type = x.m_water_type;
    m_user_offset_enabled = x.m_user_offset_enabled;
    m_zero_offset = x.m_zero_offset;
    m_zero_offset_user = x.m_zero_offset_user;

    return *this;
}

orov::DepthConfig& orov::DepthConfig::operator=(DepthConfig &&x)
{
    m_id = std::move(x.m_id);
    m_water_type = x.m_water_type;
    m_user_offset_enabled = x.m_user_offset_enabled;
    m_zero_offset = x.m_zero_offset;
    m_zero_offset_user = x.m_zero_offset_user;

    return *this;
}

size_t orov::DepthConfig::getMaxCdrSerializedSize(size_t current_alignment)
{
    size_t initial_alignment = current_alignment;

    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4) + 255 + 1;

    current_alignment += 1 + eprosima::fastcdr::Cdr::alignment(current_alignment, 1);


    current_alignment += 1 + eprosima::fastcdr::Cdr::alignment(current_alignment, 1);


    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4);


    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4);



    return current_alignment - initial_alignment;
}

size_t orov::DepthConfig::getCdrSerializedSize(const orov::DepthConfig& data, size_t current_alignment)
{
    (void)data;
    size_t initial_alignment = current_alignment;

    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4) + data.id().size() + 1;

    current_alignment += 1 + eprosima::fastcdr::Cdr::alignment(current_alignment, 1);


    current_alignment += 1 + eprosima::fastcdr::Cdr::alignment(current_alignment, 1);


    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4);


    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4);



    return current_alignment - initial_alignment;
}

void orov::DepthConfig::serialize(eprosima::fastcdr::Cdr &scdr) const
{
    scdr << m_id;
    scdr << m_water_type;
    scdr << m_user_offset_enabled;
    scdr << m_zero_offset;
    scdr << m_zero_offset_user;
}

void orov::DepthConfig::deserialize(eprosima::fastcdr::Cdr &dcdr)
{
    dcdr >> m_id;
    dcdr >> m_water_type;
    dcdr >> m_user_offset_enabled;
    dcdr >> m_zero_offset;
    dcdr >> m_zero_offset_user;
}

size_t orov::DepthConfig::getKeyMaxCdrSerializedSize(size_t current_alignment)
{
	size_t current_align = current_alignment;
            
     current_align += 4 + eprosima::fastcdr::Cdr::alignment(current_align, 4) + 255 + 1;
     





    return current_align;
}

bool orov::DepthConfig::isKeyDefined()
{
    return true;
}

void orov::DepthConfig::serializeKey(eprosima::fastcdr::Cdr &scdr) const
{
	(void) scdr;
	 scdr << m_id;  
	 
	 
	 
	 
}
