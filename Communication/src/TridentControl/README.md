### Build:

- mkdir build
- cd build
- cmake ..
- make 

### Run the Publisher and Subscriber

- inside /build directory:
    - ./TridentControl publisher
    - ./TridentControl subscriber