// Copyright 2016 Proyectos y Sistemas de Mantenimiento SL (eProsima).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*! 
 * @file Header.cpp
 * This source file contains the definition of the described types in the IDL file.
 *
 * This file was generated by the tool gen.
 */

#ifdef _WIN32
// Remove linker warning LNK4221 on Visual Studio
namespace { char dummy; }
#endif

#include "Header.h"
#include <fastcdr/Cdr.h>

#include <fastcdr/exceptions/BadParamException.h>
using namespace eprosima::fastcdr::exception;

#include <utility>

orov::msg::common::Header::Header()
{



}

orov::msg::common::Header::~Header()
{
}

orov::msg::common::Header::Header(const Header &x)
{
    m_stamp_ = x.m_stamp_;
    m_frame_id_ = x.m_frame_id_;
}

orov::msg::common::Header::Header(Header &&x)
{
    m_stamp_ = std::move(x.m_stamp_);
    m_frame_id_ = std::move(x.m_frame_id_);
}

orov::msg::common::Header& orov::msg::common::Header::operator=(const Header &x)
{
    m_stamp_ = x.m_stamp_;
    m_frame_id_ = x.m_frame_id_;

    return *this;
}

orov::msg::common::Header& orov::msg::common::Header::operator=(Header &&x)
{
    m_stamp_ = std::move(x.m_stamp_);
    m_frame_id_ = std::move(x.m_frame_id_);

    return *this;
}

size_t orov::msg::common::Header::getMaxCdrSerializedSize(size_t current_alignment)
{
    size_t initial_alignment = current_alignment;

    current_alignment += orov::msg::common::Time::getMaxCdrSerializedSize(current_alignment);
    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4) + 255 + 1;


    return current_alignment - initial_alignment;
}

size_t orov::msg::common::Header::getCdrSerializedSize(const orov::msg::common::Header& data, size_t current_alignment)
{
    (void)data;
    size_t initial_alignment = current_alignment;

    current_alignment += orov::msg::common::Time::getCdrSerializedSize(data.stamp_(), current_alignment);
    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4) + data.frame_id_().size() + 1;


    return current_alignment - initial_alignment;
}

void orov::msg::common::Header::serialize(eprosima::fastcdr::Cdr &scdr) const
{
    scdr << m_stamp_;
    scdr << m_frame_id_;
}

void orov::msg::common::Header::deserialize(eprosima::fastcdr::Cdr &dcdr)
{
    dcdr >> m_stamp_;
    dcdr >> m_frame_id_;
}

size_t orov::msg::common::Header::getKeyMaxCdrSerializedSize(size_t current_alignment)
{
	size_t current_align = current_alignment;
            



    return current_align;
}

bool orov::msg::common::Header::isKeyDefined()
{
    return false;
}

void orov::msg::common::Header::serializeKey(eprosima::fastcdr::Cdr &scdr) const
{
	(void) scdr;
	 
	 
}


