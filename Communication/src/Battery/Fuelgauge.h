// Copyright 2016 Proyectos y Sistemas de Mantenimiento SL (eProsima).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*! 
 * @file Fuelgauge.h
 * This header file contains the declaration of the described types in the IDL file.
 *
 * This file was generated by the tool gen.
 */

#ifndef _OROV_FUELGAUGE_H_
#define _OROV_FUELGAUGE_H_

// TODO Poner en el contexto.
#include "BatteryState.h"

#include <stdint.h>
#include <array>
#include <string>
#include <vector>
#include <map>
#include <bitset>

#if defined(_WIN32)
#if defined(EPROSIMA_USER_DLL_EXPORT)
#define eProsima_user_DllExport __declspec( dllexport )
#else
#define eProsima_user_DllExport
#endif
#else
#define eProsima_user_DllExport
#endif

#if defined(_WIN32)
#if defined(EPROSIMA_USER_DLL_EXPORT)
#if defined(Fuelgauge_SOURCE)
#define Fuelgauge_DllAPI __declspec( dllexport )
#else
#define Fuelgauge_DllAPI __declspec( dllimport )
#endif // Fuelgauge_SOURCE
#else
#define Fuelgauge_DllAPI
#endif
#else
#define Fuelgauge_DllAPI
#endif // _WIN32

namespace eprosima
{
    namespace fastcdr
    {
        class Cdr;
    }
}

namespace orov
{
    /*!
     * @brief This class represents the structure FuelgaugeStatus defined by the user in the IDL file.
     * @ingroup FUELGAUGE
     */
    class FuelgaugeStatus
    {
    public:

        /*!
         * @brief Default constructor.
         */
        eProsima_user_DllExport FuelgaugeStatus();

        /*!
         * @brief Default destructor.
         */
        eProsima_user_DllExport ~FuelgaugeStatus();

        /*!
         * @brief Copy constructor.
         * @param x Reference to the object orov::FuelgaugeStatus that will be copied.
         */
        eProsima_user_DllExport FuelgaugeStatus(const FuelgaugeStatus &x);

        /*!
         * @brief Move constructor.
         * @param x Reference to the object orov::FuelgaugeStatus that will be copied.
         */
        eProsima_user_DllExport FuelgaugeStatus(FuelgaugeStatus &&x);

        /*!
         * @brief Copy assignment.
         * @param x Reference to the object orov::FuelgaugeStatus that will be copied.
         */
        eProsima_user_DllExport FuelgaugeStatus& operator=(const FuelgaugeStatus &x);

        /*!
         * @brief Move assignment.
         * @param x Reference to the object orov::FuelgaugeStatus that will be copied.
         */
        eProsima_user_DllExport FuelgaugeStatus& operator=(FuelgaugeStatus &&x);

        /*!
         * @brief This function copies the value in member state
         * @param _state New value to be copied in member state
         */
        inline eProsima_user_DllExport void state(const orov::msg::sensor::BatteryState &_state)
        {
            m_state = _state;
        }

        /*!
         * @brief This function moves the value in member state
         * @param _state New value to be moved in member state
         */
        inline eProsima_user_DllExport void state(orov::msg::sensor::BatteryState &&_state)
        {
            m_state = std::move(_state);
        }

        /*!
         * @brief This function returns a constant reference to member state
         * @return Constant reference to member state
         */
        inline eProsima_user_DllExport const orov::msg::sensor::BatteryState& state() const
        {
            return m_state;
        }

        /*!
         * @brief This function returns a reference to member state
         * @return Reference to member state
         */
        inline eProsima_user_DllExport orov::msg::sensor::BatteryState& state()
        {
            return m_state;
        }
        /*!
         * @brief This function copies the value in member id
         * @param _id New value to be copied in member id
         */
        inline eProsima_user_DllExport void id(const std::string &_id)
        {
            m_id = _id;
        }

        /*!
         * @brief This function moves the value in member id
         * @param _id New value to be moved in member id
         */
        inline eProsima_user_DllExport void id(std::string &&_id)
        {
            m_id = std::move(_id);
        }

        /*!
         * @brief This function returns a constant reference to member id
         * @return Constant reference to member id
         */
        inline eProsima_user_DllExport const std::string& id() const
        {
            return m_id;
        }

        /*!
         * @brief This function returns a reference to member id
         * @return Reference to member id
         */
        inline eProsima_user_DllExport std::string& id()
        {
            return m_id;
        }
        /*!
         * @brief This function sets a value in member average_current
         * @param _average_current New value for member average_current
         */
        inline eProsima_user_DllExport void average_current(float _average_current)
        {
            m_average_current = _average_current;
        }

        /*!
         * @brief This function returns the value of member average_current
         * @return Value of member average_current
         */
        inline eProsima_user_DllExport float average_current() const
        {
            return m_average_current;
        }

        /*!
         * @brief This function returns a reference to member average_current
         * @return Reference to member average_current
         */
        inline eProsima_user_DllExport float& average_current()
        {
            return m_average_current;
        }
        /*!
         * @brief This function sets a value in member average_power
         * @param _average_power New value for member average_power
         */
        inline eProsima_user_DllExport void average_power(float _average_power)
        {
            m_average_power = _average_power;
        }

        /*!
         * @brief This function returns the value of member average_power
         * @return Value of member average_power
         */
        inline eProsima_user_DllExport float average_power() const
        {
            return m_average_power;
        }

        /*!
         * @brief This function returns a reference to member average_power
         * @return Reference to member average_power
         */
        inline eProsima_user_DllExport float& average_power()
        {
            return m_average_power;
        }
        /*!
         * @brief This function sets a value in member battery_temperature
         * @param _battery_temperature New value for member battery_temperature
         */
        inline eProsima_user_DllExport void battery_temperature(float _battery_temperature)
        {
            m_battery_temperature = _battery_temperature;
        }

        /*!
         * @brief This function returns the value of member battery_temperature
         * @return Value of member battery_temperature
         */
        inline eProsima_user_DllExport float battery_temperature() const
        {
            return m_battery_temperature;
        }

        /*!
         * @brief This function returns a reference to member battery_temperature
         * @return Reference to member battery_temperature
         */
        inline eProsima_user_DllExport float& battery_temperature()
        {
            return m_battery_temperature;
        }

        /*!
         * @brief This function returns the maximum serialized size of an object
         * depending on the buffer alignment.
         * @param current_alignment Buffer alignment.
         * @return Maximum serialized size.
         */
        eProsima_user_DllExport static size_t getMaxCdrSerializedSize(size_t current_alignment = 0);

        /*!
         * @brief This function returns the serialized size of a data depending on the buffer alignment.
         * @param data Data which is calculated its serialized size.
         * @param current_alignment Buffer alignment.
         * @return Serialized size.
         */
        eProsima_user_DllExport static size_t getCdrSerializedSize(const orov::FuelgaugeStatus& data, size_t current_alignment = 0);


        /*!
         * @brief This function serializes an object using CDR serialization.
         * @param cdr CDR serialization object.
         */
        eProsima_user_DllExport void serialize(eprosima::fastcdr::Cdr &cdr) const;

        /*!
         * @brief This function deserializes an object using CDR serialization.
         * @param cdr CDR serialization object.
         */
        eProsima_user_DllExport void deserialize(eprosima::fastcdr::Cdr &cdr);



        /*!
         * @brief This function returns the maximum serialized size of the Key of an object
         * depending on the buffer alignment.
         * @param current_alignment Buffer alignment.
         * @return Maximum serialized size.
         */
        eProsima_user_DllExport static size_t getKeyMaxCdrSerializedSize(size_t current_alignment = 0);

        /*!
         * @brief This function tells you if the Key has been defined for this type
         */
        eProsima_user_DllExport static bool isKeyDefined();

        /*!
         * @brief This function serializes the key members of an object using CDR serialization.
         * @param cdr CDR serialization object.
         */
        eProsima_user_DllExport void serializeKey(eprosima::fastcdr::Cdr &cdr) const;

    private:
        orov::msg::sensor::BatteryState m_state;
        std::string m_id;
        float m_average_current;
        float m_average_power;
        float m_battery_temperature;
    };
    /*!
     * @brief This class represents the structure FuelgaugeHealth defined by the user in the IDL file.
     * @ingroup FUELGAUGE
     */
    class FuelgaugeHealth
    {
    public:

        /*!
         * @brief Default constructor.
         */
        eProsima_user_DllExport FuelgaugeHealth();

        /*!
         * @brief Default destructor.
         */
        eProsima_user_DllExport ~FuelgaugeHealth();

        /*!
         * @brief Copy constructor.
         * @param x Reference to the object orov::FuelgaugeHealth that will be copied.
         */
        eProsima_user_DllExport FuelgaugeHealth(const FuelgaugeHealth &x);

        /*!
         * @brief Move constructor.
         * @param x Reference to the object orov::FuelgaugeHealth that will be copied.
         */
        eProsima_user_DllExport FuelgaugeHealth(FuelgaugeHealth &&x);

        /*!
         * @brief Copy assignment.
         * @param x Reference to the object orov::FuelgaugeHealth that will be copied.
         */
        eProsima_user_DllExport FuelgaugeHealth& operator=(const FuelgaugeHealth &x);

        /*!
         * @brief Move assignment.
         * @param x Reference to the object orov::FuelgaugeHealth that will be copied.
         */
        eProsima_user_DllExport FuelgaugeHealth& operator=(FuelgaugeHealth &&x);

        /*!
         * @brief This function copies the value in member state
         * @param _state New value to be copied in member state
         */
        inline eProsima_user_DllExport void state(const orov::msg::sensor::BatteryState &_state)
        {
            m_state = _state;
        }

        /*!
         * @brief This function moves the value in member state
         * @param _state New value to be moved in member state
         */
        inline eProsima_user_DllExport void state(orov::msg::sensor::BatteryState &&_state)
        {
            m_state = std::move(_state);
        }

        /*!
         * @brief This function returns a constant reference to member state
         * @return Constant reference to member state
         */
        inline eProsima_user_DllExport const orov::msg::sensor::BatteryState& state() const
        {
            return m_state;
        }

        /*!
         * @brief This function returns a reference to member state
         * @return Reference to member state
         */
        inline eProsima_user_DllExport orov::msg::sensor::BatteryState& state()
        {
            return m_state;
        }
        /*!
         * @brief This function copies the value in member id
         * @param _id New value to be copied in member id
         */
        inline eProsima_user_DllExport void id(const std::string &_id)
        {
            m_id = _id;
        }

        /*!
         * @brief This function moves the value in member id
         * @param _id New value to be moved in member id
         */
        inline eProsima_user_DllExport void id(std::string &&_id)
        {
            m_id = std::move(_id);
        }

        /*!
         * @brief This function returns a constant reference to member id
         * @return Constant reference to member id
         */
        inline eProsima_user_DllExport const std::string& id() const
        {
            return m_id;
        }

        /*!
         * @brief This function returns a reference to member id
         * @return Reference to member id
         */
        inline eProsima_user_DllExport std::string& id()
        {
            return m_id;
        }
        /*!
         * @brief This function sets a value in member full_charge_capacity
         * @param _full_charge_capacity New value for member full_charge_capacity
         */
        inline eProsima_user_DllExport void full_charge_capacity(float _full_charge_capacity)
        {
            m_full_charge_capacity = _full_charge_capacity;
        }

        /*!
         * @brief This function returns the value of member full_charge_capacity
         * @return Value of member full_charge_capacity
         */
        inline eProsima_user_DllExport float full_charge_capacity() const
        {
            return m_full_charge_capacity;
        }

        /*!
         * @brief This function returns a reference to member full_charge_capacity
         * @return Reference to member full_charge_capacity
         */
        inline eProsima_user_DllExport float& full_charge_capacity()
        {
            return m_full_charge_capacity;
        }
        /*!
         * @brief This function sets a value in member average_time_to_empty_mins
         * @param _average_time_to_empty_mins New value for member average_time_to_empty_mins
         */
        inline eProsima_user_DllExport void average_time_to_empty_mins(int32_t _average_time_to_empty_mins)
        {
            m_average_time_to_empty_mins = _average_time_to_empty_mins;
        }

        /*!
         * @brief This function returns the value of member average_time_to_empty_mins
         * @return Value of member average_time_to_empty_mins
         */
        inline eProsima_user_DllExport int32_t average_time_to_empty_mins() const
        {
            return m_average_time_to_empty_mins;
        }

        /*!
         * @brief This function returns a reference to member average_time_to_empty_mins
         * @return Reference to member average_time_to_empty_mins
         */
        inline eProsima_user_DllExport int32_t& average_time_to_empty_mins()
        {
            return m_average_time_to_empty_mins;
        }
        /*!
         * @brief This function sets a value in member cycle_count
         * @param _cycle_count New value for member cycle_count
         */
        inline eProsima_user_DllExport void cycle_count(int32_t _cycle_count)
        {
            m_cycle_count = _cycle_count;
        }

        /*!
         * @brief This function returns the value of member cycle_count
         * @return Value of member cycle_count
         */
        inline eProsima_user_DllExport int32_t cycle_count() const
        {
            return m_cycle_count;
        }

        /*!
         * @brief This function returns a reference to member cycle_count
         * @return Reference to member cycle_count
         */
        inline eProsima_user_DllExport int32_t& cycle_count()
        {
            return m_cycle_count;
        }
        /*!
         * @brief This function sets a value in member state_of_health_pct
         * @param _state_of_health_pct New value for member state_of_health_pct
         */
        inline eProsima_user_DllExport void state_of_health_pct(float _state_of_health_pct)
        {
            m_state_of_health_pct = _state_of_health_pct;
        }

        /*!
         * @brief This function returns the value of member state_of_health_pct
         * @return Value of member state_of_health_pct
         */
        inline eProsima_user_DllExport float state_of_health_pct() const
        {
            return m_state_of_health_pct;
        }

        /*!
         * @brief This function returns a reference to member state_of_health_pct
         * @return Reference to member state_of_health_pct
         */
        inline eProsima_user_DllExport float& state_of_health_pct()
        {
            return m_state_of_health_pct;
        }

        /*!
         * @brief This function returns the maximum serialized size of an object
         * depending on the buffer alignment.
         * @param current_alignment Buffer alignment.
         * @return Maximum serialized size.
         */
        eProsima_user_DllExport static size_t getMaxCdrSerializedSize(size_t current_alignment = 0);

        /*!
         * @brief This function returns the serialized size of a data depending on the buffer alignment.
         * @param data Data which is calculated its serialized size.
         * @param current_alignment Buffer alignment.
         * @return Serialized size.
         */
        eProsima_user_DllExport static size_t getCdrSerializedSize(const orov::FuelgaugeHealth& data, size_t current_alignment = 0);


        /*!
         * @brief This function serializes an object using CDR serialization.
         * @param cdr CDR serialization object.
         */
        eProsima_user_DllExport void serialize(eprosima::fastcdr::Cdr &cdr) const;

        /*!
         * @brief This function deserializes an object using CDR serialization.
         * @param cdr CDR serialization object.
         */
        eProsima_user_DllExport void deserialize(eprosima::fastcdr::Cdr &cdr);



        /*!
         * @brief This function returns the maximum serialized size of the Key of an object
         * depending on the buffer alignment.
         * @param current_alignment Buffer alignment.
         * @return Maximum serialized size.
         */
        eProsima_user_DllExport static size_t getKeyMaxCdrSerializedSize(size_t current_alignment = 0);

        /*!
         * @brief This function tells you if the Key has been defined for this type
         */
        eProsima_user_DllExport static bool isKeyDefined();

        /*!
         * @brief This function serializes the key members of an object using CDR serialization.
         * @param cdr CDR serialization object.
         */
        eProsima_user_DllExport void serializeKey(eprosima::fastcdr::Cdr &cdr) const;

    private:
        orov::msg::sensor::BatteryState m_state;
        std::string m_id;
        float m_full_charge_capacity;
        int32_t m_average_time_to_empty_mins;
        int32_t m_cycle_count;
        float m_state_of_health_pct;
    };
}

#endif // _OROV_FUELGAUGE_H_