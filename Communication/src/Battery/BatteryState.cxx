// Copyright 2016 Proyectos y Sistemas de Mantenimiento SL (eProsima).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*! 
 * @file BatteryState.cpp
 * This source file contains the definition of the described types in the IDL file.
 *
 * This file was generated by the tool gen.
 */

#ifdef _WIN32
// Remove linker warning LNK4221 on Visual Studio
namespace { char dummy; }
#endif

#include "BatteryState.h"
#include <fastcdr/Cdr.h>

#include <fastcdr/exceptions/BadParamException.h>
using namespace eprosima::fastcdr::exception;

#include <utility>






















orov::msg::sensor::BatteryState::BatteryState()
{

    m_voltage_ = 0.0;

    m_current_ = 0.0;

    m_charge_ = 0.0;

    m_capacity_ = 0.0;

    m_design_capacity_ = 0.0;

    m_percentage_ = 0.0;

    m_power_supply_status_ = 0;

    m_power_supply_health_ = 0;

    m_power_supply_technology_ = 0;

    m_present_ = false;





}

orov::msg::sensor::BatteryState::~BatteryState()
{
}

orov::msg::sensor::BatteryState::BatteryState(const BatteryState &x)
{
    m_header_ = x.m_header_;
    m_voltage_ = x.m_voltage_;
    m_current_ = x.m_current_;
    m_charge_ = x.m_charge_;
    m_capacity_ = x.m_capacity_;
    m_design_capacity_ = x.m_design_capacity_;
    m_percentage_ = x.m_percentage_;
    m_power_supply_status_ = x.m_power_supply_status_;
    m_power_supply_health_ = x.m_power_supply_health_;
    m_power_supply_technology_ = x.m_power_supply_technology_;
    m_present_ = x.m_present_;
    m_cell_voltage_ = x.m_cell_voltage_;
    m_location_ = x.m_location_;
    m_serial_number_ = x.m_serial_number_;
}

orov::msg::sensor::BatteryState::BatteryState(BatteryState &&x)
{
    m_header_ = std::move(x.m_header_);
    m_voltage_ = x.m_voltage_;
    m_current_ = x.m_current_;
    m_charge_ = x.m_charge_;
    m_capacity_ = x.m_capacity_;
    m_design_capacity_ = x.m_design_capacity_;
    m_percentage_ = x.m_percentage_;
    m_power_supply_status_ = x.m_power_supply_status_;
    m_power_supply_health_ = x.m_power_supply_health_;
    m_power_supply_technology_ = x.m_power_supply_technology_;
    m_present_ = x.m_present_;
    m_cell_voltage_ = std::move(x.m_cell_voltage_);
    m_location_ = std::move(x.m_location_);
    m_serial_number_ = std::move(x.m_serial_number_);
}

orov::msg::sensor::BatteryState& orov::msg::sensor::BatteryState::operator=(const BatteryState &x)
{
    m_header_ = x.m_header_;
    m_voltage_ = x.m_voltage_;
    m_current_ = x.m_current_;
    m_charge_ = x.m_charge_;
    m_capacity_ = x.m_capacity_;
    m_design_capacity_ = x.m_design_capacity_;
    m_percentage_ = x.m_percentage_;
    m_power_supply_status_ = x.m_power_supply_status_;
    m_power_supply_health_ = x.m_power_supply_health_;
    m_power_supply_technology_ = x.m_power_supply_technology_;
    m_present_ = x.m_present_;
    m_cell_voltage_ = x.m_cell_voltage_;
    m_location_ = x.m_location_;
    m_serial_number_ = x.m_serial_number_;

    return *this;
}

orov::msg::sensor::BatteryState& orov::msg::sensor::BatteryState::operator=(BatteryState &&x)
{
    m_header_ = std::move(x.m_header_);
    m_voltage_ = x.m_voltage_;
    m_current_ = x.m_current_;
    m_charge_ = x.m_charge_;
    m_capacity_ = x.m_capacity_;
    m_design_capacity_ = x.m_design_capacity_;
    m_percentage_ = x.m_percentage_;
    m_power_supply_status_ = x.m_power_supply_status_;
    m_power_supply_health_ = x.m_power_supply_health_;
    m_power_supply_technology_ = x.m_power_supply_technology_;
    m_present_ = x.m_present_;
    m_cell_voltage_ = std::move(x.m_cell_voltage_);
    m_location_ = std::move(x.m_location_);
    m_serial_number_ = std::move(x.m_serial_number_);

    return *this;
}

size_t orov::msg::sensor::BatteryState::getMaxCdrSerializedSize(size_t current_alignment)
{
    size_t initial_alignment = current_alignment;

    current_alignment += orov::msg::common::Header::getMaxCdrSerializedSize(current_alignment);
    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4);


    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4);


    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4);


    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4);


    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4);


    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4);


    current_alignment += 1 + eprosima::fastcdr::Cdr::alignment(current_alignment, 1);


    current_alignment += 1 + eprosima::fastcdr::Cdr::alignment(current_alignment, 1);


    current_alignment += 1 + eprosima::fastcdr::Cdr::alignment(current_alignment, 1);


    current_alignment += 1 + eprosima::fastcdr::Cdr::alignment(current_alignment, 1);


    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4);

    current_alignment += (100 * 4) + eprosima::fastcdr::Cdr::alignment(current_alignment, 4);



    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4) + 255 + 1;

    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4) + 255 + 1;


    return current_alignment - initial_alignment;
}

size_t orov::msg::sensor::BatteryState::getCdrSerializedSize(const orov::msg::sensor::BatteryState& data, size_t current_alignment)
{
    (void)data;
    size_t initial_alignment = current_alignment;

    current_alignment += orov::msg::common::Header::getCdrSerializedSize(data.header_(), current_alignment);
    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4);


    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4);


    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4);


    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4);


    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4);


    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4);


    current_alignment += 1 + eprosima::fastcdr::Cdr::alignment(current_alignment, 1);


    current_alignment += 1 + eprosima::fastcdr::Cdr::alignment(current_alignment, 1);


    current_alignment += 1 + eprosima::fastcdr::Cdr::alignment(current_alignment, 1);


    current_alignment += 1 + eprosima::fastcdr::Cdr::alignment(current_alignment, 1);


    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4);

    current_alignment += (data.cell_voltage_().size() * 4) + eprosima::fastcdr::Cdr::alignment(current_alignment, 4);



    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4) + data.location_().size() + 1;

    current_alignment += 4 + eprosima::fastcdr::Cdr::alignment(current_alignment, 4) + data.serial_number_().size() + 1;


    return current_alignment - initial_alignment;
}

void orov::msg::sensor::BatteryState::serialize(eprosima::fastcdr::Cdr &scdr) const
{
    scdr << m_header_;
    scdr << m_voltage_;
    scdr << m_current_;
    scdr << m_charge_;
    scdr << m_capacity_;
    scdr << m_design_capacity_;
    scdr << m_percentage_;
    scdr << m_power_supply_status_;
    scdr << m_power_supply_health_;
    scdr << m_power_supply_technology_;
    scdr << m_present_;
    scdr << m_cell_voltage_;
    scdr << m_location_;
    scdr << m_serial_number_;
}

void orov::msg::sensor::BatteryState::deserialize(eprosima::fastcdr::Cdr &dcdr)
{
    dcdr >> m_header_;
    dcdr >> m_voltage_;
    dcdr >> m_current_;
    dcdr >> m_charge_;
    dcdr >> m_capacity_;
    dcdr >> m_design_capacity_;
    dcdr >> m_percentage_;
    dcdr >> m_power_supply_status_;
    dcdr >> m_power_supply_health_;
    dcdr >> m_power_supply_technology_;
    dcdr >> m_present_;
    dcdr >> m_cell_voltage_;
    dcdr >> m_location_;
    dcdr >> m_serial_number_;
}

size_t orov::msg::sensor::BatteryState::getKeyMaxCdrSerializedSize(size_t current_alignment)
{
	size_t current_align = current_alignment;
            















    return current_align;
}

bool orov::msg::sensor::BatteryState::isKeyDefined()
{
    return false;
}

void orov::msg::sensor::BatteryState::serializeKey(eprosima::fastcdr::Cdr &scdr) const
{
	(void) scdr;
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
}


