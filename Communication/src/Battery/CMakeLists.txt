
cmake_minimum_required(VERSION 2.8.12)

project("generated_code")

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_EXTENSIONS OFF)

# Find requirements
find_package(fastcdr REQUIRED)
find_package(fastrtps REQUIRED)


message(STATUS "Configuring Time...")
add_library(Time_lib Time.cxx)
target_link_libraries(Time_lib fastcdr fastrtps)

#add_executable(Time TimePubSubTypes.cxx TimePublisher.cxx TimeSubscriber.cxx TimePubSubMain.cxx)
#target_link_libraries(Time fastcdr fastrtps
#        Time_lib )


message(STATUS "Configuring Header...")
add_library(Header_lib Header.cxx)
target_link_libraries(Header_lib fastcdr fastrtps)

#add_executable(Header HeaderPubSubTypes.cxx HeaderPublisher.cxx HeaderSubscriber.cxx HeaderPubSubMain.cxx)
#target_link_libraries(Header fastcdr fastrtps
#        Header_lib Time_lib)


message(STATUS "Configuring BatteryState...")
add_library(BatteryState_lib BatteryState.cxx)
target_link_libraries(BatteryState_lib fastcdr fastrtps)

#add_executable(BatteryState BatteryStatePubSubTypes.cxx BatteryStatePublisher.cxx BatteryStateSubscriber.cxx BatteryStatePubSubMain.cxx)
#target_link_libraries(BatteryState fastcdr fastrtps
#        BatteryState_lib Header_lib Time_lib)


message(STATUS "Configuring Fuelgauge...")
add_library(Fuelgauge_lib Fuelgauge.cxx)
target_link_libraries(Fuelgauge_lib fastcdr fastrtps)

add_executable(Fuelgauge FuelgaugePubSubTypes.cxx FuelgaugePublisher.cxx FuelgaugeSubscriber.cxx FuelgaugePubSubMain.cxx)
target_link_libraries(Fuelgauge fastcdr fastrtps
        Fuelgauge_lib BatteryState_lib Header_lib Time_lib)

