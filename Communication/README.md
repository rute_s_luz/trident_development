# Trident Communication

###### This component will serve as communication "drivers" for the integration of Trident with new the components.

#### "idl" folder:
- message definion (.idl files).

#### "src" folder:
- Subscribers and Publishers for Trident messages
- *.cxx and *.h files containing Subscribers and Publishers for which message type (*.idl file)
- this files can be automatically generated with "fastrtpsgen -example CMake <Path_to_idl_file>"
     - After automatic generation of the *.cxx and *.h files, these ones should modified to accomplishe the intenteded goals (Default example publishes a message when 'y' key is pressed and prints information regarding new message from the subscriber).
    - Example to generate pub and sub for the motor control of Trident: 
fastrtpsgen -example CMake ../../idl/control/TridentControl.idl

- It was not generated publishers and subscribers to all *.idl messages, these will be generated as they become necessary.


## Dependencies
- To generate pubs and subs is necessary to install FastRTPS 2 (by ePromisa):
    - https://github.com/eProsima/Fast-RTPS.git
    - To Install from Source:
        - Ensure that libgstreamer1.0-dev and libgstreamermm-1.0-dev are installed

        
    `git clone https://github.com/eProsima/Fast-RTPS.git`
    
    `cd Fast-RTPS`
    
    `mkdir build`
    
    `cd build`
    
    `cmake -DTHIRDPARTY=ON -DBUILD_JAVA=ON -DCOMPILE_EXAMPLES=ON -DPERFORMANCE_TESTS=ON … `
    
    `make`
    
    `sudo make install`
